CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Installation
 * Configuration


INTRODUCTION
------------

Current Maintainer: Nicolas Bouhid <nicolasbouhid@gmail.com>

This module provides an extensible view for deployment plans listing. It is
intended to provide more flexibility and information on the deploy overview
page.

 * For a full description of the module, visit the project page:
   https://www.drupal.org/sandbox/nbouhid/2615920

 * To submit bug reports and feature suggestions, or to track changes:
   https://www.drupal.org/project/issues/2615920


REQUIREMENTS
------------

This module requires the following modules:

 * Views (https://drupal.org/project/views)
 * Deploy UI (https://drupal.org/project/deploy)


INSTALLATION
------------

 * Install as you would normally install a contributed Drupal module. See:
   https://drupal.org/documentation/install/modules-themes/modules-7
   for further information.


CONFIGURATION
-------------

 * You can configure the view deploy_overview_views on Administration »
   Structure » Views and click edit next to the view:

   - Remove fields that you don't need.

   - Rename anything on the view.

   - Filter content.

 * If you are a developer, you can:

   - Create new views handlers for new relationships or entity information.

   - Reuse the view somewhere else
