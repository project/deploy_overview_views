<?php
/**
 * @file
 * deploy_overview_views.pages.inc
 */

/**
 * Page callback for the deploy overview page.
 */
function deploy_overview_views_overview_page() {
  $plans = deploy_plan_load_all_enabled();
  $blocks = array();

  // Iterate over all plans.
  foreach ($plans as $plan) {
    // Construct a usable array for the theme function.
    $blocks[] = array(
      'plan_name' => check_plain($plan->name),
      'plan_title' => check_plain($plan->title),
      'plan_description' => check_plain($plan->description),
      'content' => _deploy_overview_views_plan_detail_view($plan->name),
      'fetch_only' => $plan->fetch_only,
      'status' => deploy_plan_get_status($plan->name),
    );
  }

  return theme('deploy_ui_overview', array('blocks' => $blocks));
}

/**
 * Helper to load and render the plan details view.
 */
function _deploy_overview_views_plan_detail_view($plan_name) {
  return views_embed_view('deploy_overview_views', 'block', $plan_name);
}
