<?php
/**
 * @file
 * Views integration file for deploy_overview_views.
 */

/**
 * Implements hook_views_data().
 */
function deploy_overview_views_views_data() {
  $data = array();

  $data['deploy_manager_entities']['table']['group'] = t('Deploy');

  $data['deploy_manager_entities']['table']['base'] = array(
    'title' => t('Deploy manager entities'),
    'help' => t('Contains entities referenced in deployment plans.'),
  );

  $data['deploy_manager_entities']['plan_name'] = array(
    'title' => t('Plan name'),
    'help' => t('The deployment plan name.'),
    'field' => array(
      'handler' => 'views_handler_field_string',
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_string',
    ),
    'argument' => array(
      'handler' => 'views_handler_argument_string',
    ),
  );

  $data['deploy_manager_entities']['entity_type'] = array(
    'title' => t('Entity type'),
    'help' => t('The type of the entity to be deployed.'),
    'field' => array(
      'handler' => 'views_handler_field',
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_string',
    ),
  );

  $data['deploy_manager_entities']['entity_id'] = array(
    'title' => t('Entity ID'),
    'help' => t('The id of the entity to be deployed.'),
    'field' => array(
      'handler' => 'views_handler_field',
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_numeric',
    ),
  );

  $data['deploy_manager_entities']['revision_id'] = array(
    'title' => t('Revision ID'),
    'help' => t('The revision ID of the entity to be deployed.'),
    'field' => array(
      'handler' => 'views_handler_field',
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_numeric',
    ),
  );

  $data['deploy_manager_entities']['timestamp'] = array(
    'title' => t('Date when added to plan'),
    'help' => t('The date when the entity was added to the plan.'),
    'field' => array(
      'handler' => 'deploy_overview_views_handler_field_date_microtime',
    ),
  );

  // Pseudo fields definition.
  $pseudo_field_base = array(
    'real field' => 'entity_id',
    'field' => array(
      'additional fields' => array(
        'entity_type' => array(
          'table' => 'deploy_manager_entities',
          'field' => 'entity_type',
        ),
        'revision_id' => array(
          'table' => 'deploy_manager_entities',
          'field' => 'revision_id',
        ),
      ),
    ),
  );
  $data['deploy_manager_entities']['entity'] = array_merge_recursive($pseudo_field_base, array(
    'title' => 'Entity to be deployed',
    'help' => t('The label of the entity to be deployed.'),
    'field' => array(
      'handler' => 'deploy_overview_views_handler_field_entity',
    ),
  ));

  $data['deploy_manager_entities']['entity_bundle'] = array_merge_recursive($pseudo_field_base, array(
    'title' => 'Entity bundle',
    'help' => t('The bundle of the entity to be deployed.'),
    'field' => array(
      'handler' => 'deploy_overview_views_handler_field_entity_bundle',
    ),
  ));

  $data['deploy_manager_entities']['entity_language'] = array_merge_recursive($pseudo_field_base, array(
    'title' => 'Language',
    'help' => t('The language of the entity to be deployed.'),
    'field' => array(
      'handler' => 'deploy_overview_views_handler_field_entity_language',
    ),
  ));

  $data['deploy_manager_entities']['entity_remove'] = array_merge_recursive($pseudo_field_base, array(
    'title' => 'Remove from plan link',
    'help' => t('A link that removes the entity to be deployed.'),
    'field' => array(
      'handler' => 'deploy_overview_views_handler_field_entity_remove',
      'additional fields' => array(
        'plan_name' => array(
          'table' => 'deploy_manager_entities',
          'field' => 'plan_name',
        ),
      ),
    ),
  ));

  return $data;
}
