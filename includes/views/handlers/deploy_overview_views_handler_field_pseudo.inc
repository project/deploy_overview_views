<?php
/**
 * @file
 * Contains the deploy_overview_views_handler_field_pseudo class.
 */

/**
 * Abstract class for pseudo fields defined by the deploy_overview_views module.
 */
abstract class deploy_overview_views_handler_field_pseudo extends views_handler_field {

  /**
   * Stores the result.
   */
  protected $result = array();

  /**
   * Callback function to process the current entity.
   */
  protected function process_entity($entity, $info) {}

  /**
   * Prepares the result to be rendered.
   */
  public function pre_render(&$values) {
    if (empty($values)) {
      return;
    }

    $this->result = array();
    foreach ($values as $value) {
      $entity_id = $value->{$this->field_alias};
      $entity_type = $value->{$this->aliases['entity_type']};
      $revision_id = $value->{$this->aliases['revision_id']};

      // Get the entity info and all entities of this type.
      $entity_info = entity_get_info($entity_type);

      if (!empty($entity_info['entity keys']['revision']) && !empty($revision_id)) {
        $entity = entity_revision_load($entity_type, $revision_id);
      }
      else {
        $entity = entity_load_single($entity_type, $entity_id);
      }

      $this->result[$entity_id] = $this->process_entity($entity, array(
        'entity_id' => $entity_id,
        'entity_type' => $entity_type,
        'revision_id' => $revision_id,
      ));
    }
  }

  /**
   * Renders the output.
   */
  public function render($values) {
    $entity_id = $this->get_value($values);

    return $this->result[$entity_id];
  }

}
