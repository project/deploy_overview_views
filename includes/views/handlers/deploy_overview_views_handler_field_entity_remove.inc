<?php
/**
 * @file
 * Contains the deploy_overview_views_handler_field_entity_remove class.
 */

/**
 * Field handler that provides a form to remove entities from a deployment plan.
 */
class deploy_overview_views_handler_field_entity_remove extends views_handler_field {
  /**
   * Defining form.
   */
  public function views_form(&$form, &$form_state) {
    // Hide the form if there are no results.
    if (empty($this->view->result)) {
      $form['#access'] = FALSE;
    }

    $form[$this->options['id']] = array(
      '#type' => 'container',
      '#tree' => TRUE,
    );

    foreach ($this->view->result as $row_index => $row) {
      $form[$this->options['id']][$row_index] = array(
        '#type' => 'checkbox',
        '#default_value' => FALSE,
      );
    }

    // Submit to the current page if not a page display.
    if ($this->view->display_handler->plugin_name != 'page') {
      $form['#action'] = url(current_path());
    }
  }

  /**
   * Form submit handler.
   */
  public function views_form_submit($form, &$form_state) {
    $count = 0;
    foreach ($this->view->result as $row_index => $row) {
      $remove = $form_state['values'][$this->options['id']][$row_index];

      if (!empty($remove)) {
        $entity_type = $row->{$this->aliases['entity_type']};
        $entity = entity_load_single($entity_type, $row->{$this->field_alias});
        $plan_name = $row->{$this->aliases['plan_name']};

        deploy_manager_delete_from_plan($plan_name, $entity_type, $entity);
        $count++;
      }
    }

    $message = format_plural($count, 'An entity was removed from the plan.', '@count entities were removed from the plan.');

    drupal_set_message($message);
  }

  /**
   * Renders the form item.
   */
  public function render($values) {
    return '<!--form-item-' . $this->options['id'] . '--' . $this->view->row_index . '-->';
  }

}
