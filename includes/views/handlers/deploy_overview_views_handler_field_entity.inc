<?php
/**
 * @file
 * Contains the deploy_overview_views_handler_field_entity class.
 */

/**
 * A handler to display the entity label as a link when possible.
 */
class deploy_overview_views_handler_field_entity extends deploy_overview_views_handler_field_pseudo {
  /**
   * Generates the title as a link when possible.
   */
  protected function process_entity($entity, $info) {
    $title = "{$info['entity_type']}:{$info['entity_id']}";
    $label = entity_label($info['entity_type'], $entity);
    if ($label) {
      $title = $label;
    }

    $entity_info = entity_get_info($info['entity_type']);
    if ($entity_info['entity keys']['revision'] && !empty($info['revision_id'])) {
      $title = t('@title (rev:@rev_id)', array('@title' => $title, '@rev_id' => $info['revision_id']));
    }

    // Some entities fail fatally with entity_uri() and
    // entity_extract_ids(). So handle this gracefully.
    try {
      $uri = entity_uri($info['entity_type'], $entity);
      if ($uri) {
        $title = l($title, $uri['path'], $uri['options']);
      }
    }
    catch (Exception $e) {
      watchdog_exception('deploy_overview_views', $e);
    }

    return $title;
  }

}
