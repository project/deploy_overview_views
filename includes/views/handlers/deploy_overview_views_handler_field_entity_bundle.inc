<?php
/**
 * @file
 * Contains the deploy_overview_views_handler_field_entity_bundle class.
 */

/**
 * Field handler that provides the bundle of the entity to be deployed.
 */
class deploy_overview_views_handler_field_entity_bundle extends deploy_overview_views_handler_field_pseudo {
  /**
   * Wraps the entity and returns the bundle.
   */
  protected function process_entity($entity, $info) {
    $wrapper = entity_metadata_wrapper($info['entity_type'], $entity);

    return $wrapper->getBundle();
  }

}
