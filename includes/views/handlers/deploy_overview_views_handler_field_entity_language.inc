<?php
/**
 * @file
 * Contains the deploy_overview_views_handler_field_entity_language class.
 */

/**
 * Field handler used to get the entity_language of the entity to be deployed.
 */
class deploy_overview_views_handler_field_entity_language extends deploy_overview_views_handler_field_pseudo {

  /**
   * Contains all the languages.
   *
   * @see language_list()
   */
  protected $language_list = array();

  /**
   * Gets the entity language and returns it as a human readable string.
   */
  protected function process_entity($entity, $info) {
    $language = entity_language($info['entity_type'], $entity);

    return $this->language_list[$language]->name;
  }

  /**
   * Populates the language_list variable.
   */
  public function pre_render(&$values) {
    if (empty($values)) {
      return;
    }

    // Adding the LANGUAGE_NONE language.
    $this->language_list = array_merge(language_list(), array(
      LANGUAGE_NONE => (object) array(
        'name' => t('Language neutral'),
      ),
    ));

    parent::pre_render($values);
  }

}
