<?php
/**
 * @file
 * Contains the deploy_overview_views_handler_field_date_microtime class.
 */

/**
 * Adaptor for views_handler_field_data that transforms microtime to timestamp.
 */
class deploy_overview_views_handler_field_date_microtime extends views_handler_field_date {
  /**
   * Converts dates from microtime to timestamp.
   */
  public function pre_render(&$values) {
    foreach ($values as &$value) {
      $old_value = $value->{$this->field_alias};

      $splitted_value = explode('.', $old_value);

      $value->{$this->field_alias} = $splitted_value[0];
    }
  }

}
