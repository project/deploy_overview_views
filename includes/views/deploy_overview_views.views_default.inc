<?php
/**
 * @file
 * deploy_overview_views.views_default.inc
 */

/**
 * Implements hook_views_default_views().
 */
function deploy_overview_views_views_default_views() {
  $export = array();

  $view = new view();
  $view->name = 'deploy_overview_views';
  $view->description = '';
  $view->tag = 'default';
  $view->base_table = 'deploy_manager_entities';
  $view->human_name = 'Deployment overview';
  $view->core = 7;
  $view->api_version = '3.0';
  $view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

  /* Display: Master */
  $handler = $view->new_display('default', 'Master', 'default');
  $handler->display->display_options['use_more_always'] = FALSE;
  $handler->display->display_options['access']['type'] = 'perm';
  $handler->display->display_options['access']['perm'] = 'administer deployments';
  $handler->display->display_options['cache']['type'] = 'none';
  $handler->display->display_options['query']['type'] = 'views_query';
  $handler->display->display_options['exposed_form']['type'] = 'basic';
  $handler->display->display_options['pager']['type'] = 'none';
  $handler->display->display_options['style_plugin'] = 'table';
  $handler->display->display_options['style_options']['grouping'] = array(
    0 => array(
      'field' => 'plan_name',
      'rendered' => 0,
      'rendered_strip' => 0,
    ),
  );
  $handler->display->display_options['style_options']['columns'] = array(
    'plan_name' => 'plan_name',
    'entity' => 'entity',
    'timestamp' => 'timestamp',
    'entity_language' => 'entity_language',
  );
  $handler->display->display_options['style_options']['default'] = '-1';
  $handler->display->display_options['style_options']['info'] = array(
    'plan_name' => array(
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'entity' => array(
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'timestamp' => array(
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'entity_language' => array(
      'align' => '',
      'separator' => '',
      'empty_column' => 1,
    ),
  );
  $handler->display->display_options['style_options']['sticky'] = TRUE;
  $handler->display->display_options['style_options']['empty_table'] = TRUE;
  /* No results behavior: Global: Text area */
  $handler->display->display_options['empty']['area']['id'] = 'area';
  $handler->display->display_options['empty']['area']['table'] = 'views';
  $handler->display->display_options['empty']['area']['field'] = 'area';
  $handler->display->display_options['empty']['area']['label'] = 'No results text';
  $handler->display->display_options['empty']['area']['empty'] = TRUE;
  $handler->display->display_options['empty']['area']['content'] = '<p>Currently no content in this plan.</p>';
  $handler->display->display_options['empty']['area']['format'] = 'filtered_html';
  /* Field: Deploy: Remove from plan link */
  $handler->display->display_options['fields']['entity_remove']['id'] = 'entity_remove';
  $handler->display->display_options['fields']['entity_remove']['table'] = 'deploy_manager_entities';
  $handler->display->display_options['fields']['entity_remove']['field'] = 'entity_remove';
  $handler->display->display_options['fields']['entity_remove']['label'] = 'Remove';
  /* Field: Deploy: Entity to be deployed */
  $handler->display->display_options['fields']['entity']['id'] = 'entity';
  $handler->display->display_options['fields']['entity']['table'] = 'deploy_manager_entities';
  $handler->display->display_options['fields']['entity']['field'] = 'entity';
  $handler->display->display_options['fields']['entity']['label'] = 'Entity';
  /* Field: Deploy: Entity type */
  $handler->display->display_options['fields']['entity_type']['id'] = 'entity_type';
  $handler->display->display_options['fields']['entity_type']['table'] = 'deploy_manager_entities';
  $handler->display->display_options['fields']['entity_type']['field'] = 'entity_type';
  $handler->display->display_options['fields']['entity_type']['label'] = 'Type';
  /* Field: Deploy: Entity bundle */
  $handler->display->display_options['fields']['entity_bundle_1']['id'] = 'entity_bundle_1';
  $handler->display->display_options['fields']['entity_bundle_1']['table'] = 'deploy_manager_entities';
  $handler->display->display_options['fields']['entity_bundle_1']['field'] = 'entity_bundle';
  $handler->display->display_options['fields']['entity_bundle_1']['label'] = 'Bundle';
  /* Field: Deploy: Language */
  $handler->display->display_options['fields']['entity_language']['id'] = 'entity_language';
  $handler->display->display_options['fields']['entity_language']['table'] = 'deploy_manager_entities';
  $handler->display->display_options['fields']['entity_language']['field'] = 'entity_language';
  /* Field: Deploy: Date when added to plan */
  $handler->display->display_options['fields']['timestamp']['id'] = 'timestamp';
  $handler->display->display_options['fields']['timestamp']['table'] = 'deploy_manager_entities';
  $handler->display->display_options['fields']['timestamp']['field'] = 'timestamp';
  $handler->display->display_options['fields']['timestamp']['date_format'] = 'long';
  $handler->display->display_options['fields']['timestamp']['second_date_format'] = 'long';
  /* Contextual filter: Deploy: Plan name */
  $handler->display->display_options['arguments']['plan_name']['id'] = 'plan_name';
  $handler->display->display_options['arguments']['plan_name']['table'] = 'deploy_manager_entities';
  $handler->display->display_options['arguments']['plan_name']['field'] = 'plan_name';
  $handler->display->display_options['arguments']['plan_name']['default_action'] = 'not found';
  $handler->display->display_options['arguments']['plan_name']['default_argument_type'] = 'fixed';
  $handler->display->display_options['arguments']['plan_name']['summary']['number_of_records'] = '0';
  $handler->display->display_options['arguments']['plan_name']['summary']['format'] = 'default_summary';
  $handler->display->display_options['arguments']['plan_name']['summary_options']['items_per_page'] = '25';
  $handler->display->display_options['arguments']['plan_name']['limit'] = '0';

  /* Display: Block */
  $handler = $view->new_display('block', 'Block', 'block');
  $export['deploy_overview_views'] = $view;

  return $export;
}
